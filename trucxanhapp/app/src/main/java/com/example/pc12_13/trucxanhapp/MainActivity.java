package com.example.pc12_13.trucxanhapp;


import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioGroup;

import com.example.pc12_13.trucxanhapp.fragment.fragment_play;

public class MainActivity extends AppCompatActivity {
    Button btnPlay;
    RadioGroup rdg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rdg = (RadioGroup)findViewById(R.id.rdogroup);

        btnPlay = (Button) findViewById(R.id.btnplay);

        btnPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int check = rdg.getCheckedRadioButtonId();
                int flag = 0;
                System.out.println(check);
                if(check == R.id.radioButton_de){
                    flag = 1;
                }else {
                    flag = 2;
                }
                final int finalFlag = flag;
                Intent it = new Intent(MainActivity.this,playmain.class);
                it.putExtra("flag", finalFlag);
               startActivity(it);

            }
        });
    }


}
